<?php

use App\Http\Controllers\EventController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1/events', 'middleware' => 'auth.jwt'], function () {
  Route::get('', [EventController::class, 'index']);
  Route::post('', [EventController::class, 'store']);
  Route::get('/by-user/{id}', [EventController::class, 'byUser']);
});
