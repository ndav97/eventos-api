<?php

use App\Http\Controllers\SuscriptionController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1/suscriptions', 'middleware' => 'auth.jwt'], function () {
  Route::post('', [SuscriptionController::class, 'store']);
  Route::get('/by-user/{user_id}', [SuscriptionController::class, 'byUser']);
});
