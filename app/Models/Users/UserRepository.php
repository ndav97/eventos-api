<?php

namespace App\Models\Users;

class UserRepository
{
  protected $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }

  /**
   *  Crea un nuevo usuario
   */
  public function create($data): ?User
  {
    [$rules, $messages] = $this->validate($data);
    CheckValidate($data, $rules, $messages);
    $user = new User($data);
    $user->save();
    return $user;
  }

  /**
   *  Encuentra un usuario segun su id
   */
  public function findById($id): ?User
  {
    $user = $this->user->find($id);

    return $user;
  }

  /**
   * Encontrar un usuario por su nombre de usuario
   */
  public function findByCorreo($email)
  {
    $user = $this->user->where('email', $email)
      ->first();

    return $user;
  }

  /**
   * Validar la contraseña de un usuario
   */
  public function validatePassword($email, $password)
  {
    $user = $this->user->where('email', $email)
      ->whereRaw("password = PASSWORD('$password')")
      ->first();

    return $user;
  }

  /**
   * Validar la información que se recibe para el login y creación de un usuario
   */
  public function validate()
  {
    $rules = [
      'email' => 'required',
      'password' => 'required'
    ];

    $messages = [
      'email.required' => 'Correo obligatorio',
      'password.required' => 'Contraseña obligatoria'
    ];

    return [$rules, $messages];
  }
}
