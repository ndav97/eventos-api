<?php

namespace App\Models\Users;

use Exception;
use Illuminate\Support\Facades\Hash;

class UserObserver
{
    public function creating(User $user)
    {
        $user->password = md5($user->password);
    }
}
