<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
  protected $table = 'users';

  protected $hidden = ['password'];

  protected $fillable = [
    'email',
    'password',
  ];

  protected static function boot()
  {
    parent::boot();
    User::observe(UserObserver::class);
  }
    /**
   * Verficar que la contraseña del tercero sea válida
   */
  public function validPassword($password)
  {
    $encriptado = md5( $password );
    return $this->password == $encriptado;
  }
}
