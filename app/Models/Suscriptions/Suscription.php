<?php

namespace App\Models\Suscriptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Suscription extends Model
{
  protected $table = 'suscriptions';

  public $timestamps = false;

  protected $fillable = [
    'user_id',
    'event_id',
  ];
}
