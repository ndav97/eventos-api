<?php

namespace App\Models\Suscriptions;

class SuscriptionRepository
{
  protected $suscription;

  public function __construct(Suscription $suscription)
  {
    $this->suscription = $suscription;
  }

  /**
   *  Encuentra una suscripción segun su id
   */
  public function findById($id): ?Suscription
  {
    $suscription = $this->suscription->find($id);

    return $suscription;
  }

  /**
   *  Encuentra las suscripciones de un usuario
   */
  public function findByUserId($user_id)
  {
    $suscriptions = $this->suscription->where('user_id', $user_id)->get();

    return $suscriptions;
  }

  /**
   *  Registra una suscripción
   */
  public function create($data): ?Suscription
  {
    [$rules, $messages] = $this->validate($data);
    CheckValidate($data, $rules, $messages);
    $suscription = new Suscription($data);
    $suscription->save();
    return $suscription;
  }

  /**
   * Validar la información que se recibe para la creación de eventos
   */
  public function validate()
  {
    $rules = [
      'user_id' => 'required',
      'event_id' => 'required'
    ];

    $messages = [
      'user_id.required' => 'Usuario obligatorio',
      'event_id.required' => 'Evento obligatorio',
    ];

    return [$rules, $messages];
  }
}
