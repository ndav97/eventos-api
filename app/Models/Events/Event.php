<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Event extends Model
{
  protected $table = 'events';

  protected $fillable = [
    'nombre',
    'lugar',
    'fecha',
    'user_id',
  ];
}
