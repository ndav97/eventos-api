<?php

namespace App\Models\Events;

class EventRepository
{
  protected $event;

  public function __construct(Event $event)
  {
    $this->event = $event;
  }

  /**
   *  Obtiene todos los eventos
   */
  public function index()
  {
    $events = $this->event->get();

    return $events;
  }

  /**
   *  Encuentra un usuario segun su id
   */
  public function findById($id): ?Event
  {
    $event = $this->event->find($id);

    return $event;
  }

  /**
   *  Encuentra los eventos de un usuario
   */
  public function findByUserId($id)
  {
    $events = $this->event->where('user_id', $id)->get();

    return $events;
  }

  /**
   *  Crea un nuevo evento
   */
  public function create($data): ?Event
  {
    [$rules, $messages] = $this->validate($data);
    CheckValidate($data, $rules, $messages);
    $event = new Event($data);
    $event->save();
    return $event;
  }

  /**
   * Validar la información que se recibe para la creación de eventos
   */
  public function validate()
  {
    $rules = [
      'nombre' => 'required',
      'lugar' => 'required',
      'fecha' => 'required'
    ];

    $messages = [
      'nombre.required' => 'Nombre de evento obligatorio',
      'lugar.required' => 'Lugar de evento obligatorio',
      'fecha.required' => 'Fecha de evento obligatoria'
    ];

    return [$rules, $messages];
  }
}
