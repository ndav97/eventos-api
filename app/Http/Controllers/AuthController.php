<?php

namespace App\Http\Controllers;

use App\Models\Users\UserRepository;
use Firebase\JWT\JWT;
use Illuminate\Http\Response;

class AuthController extends Controller
{
  protected $userRepository;

  public function __construct(UserRepository $userRepository)
  {
    $this->userRepository = $userRepository;
  }

  protected function generateToken(Int $id)
  {
    $payload = [
      'iss' => env('JWT_ISS'),
      'id' => $id,
      'iat' => time(),
      // 'exp' => time() + (24 * 10 * 60),
    ];

    return JWT::encode($payload, env('JWT_SECRET'));
  }

  /**
   * Login usuario
   */
  public function login()
  {
    $dataLogin = request()->input();
    [$rules, $messages] = $this->userRepository->validate($dataLogin);
    CheckValidate($dataLogin, $rules, $messages);

    $email = $dataLogin['email'];
    $password = $dataLogin['password'];

    $user = $this->userRepository->findByCorreo($email);

    CheckModel($user, 'Correo no registrado');

    $passwordValid = $user->validPassword($password);

    if (!$passwordValid) {
      ThrowBadRequest('La contraseña es incorrecta');
    }

    return response([
      'user' => $user,
      'token' => $this->generateToken($user->id)
    ], Response::HTTP_OK);
  }

  /**
   * Registro de usuario
   */
  public function registro()
  {
    $dataRegistro = request()->input();

    $userExist = $this->userRepository->findByCorreo($dataRegistro['email']);

    if($userExist) {
      return response([
        'message' => 'El Correo ya se encuentra registrado'
      ], 400);
    }

    $user = $this->userRepository->create($dataRegistro);

    return response([
      'user' => $user,
      'token' => $this->generateToken($user->id)
    ], Response::HTTP_OK);
  }
}
