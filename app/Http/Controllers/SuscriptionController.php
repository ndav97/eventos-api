<?php

namespace App\Http\Controllers;

use App\Models\Suscriptions\SuscriptionRepository;
use Firebase\JWT\JWT;
use Illuminate\Http\Response;

class SuscriptionController extends Controller
{
  protected $suscriptionRepository;

  public function __construct(SuscriptionRepository $suscriptionRepository)
  {
    $this->suscriptionRepository = $suscriptionRepository;
  }

  /**
   * Registro de una suscripción
   */
  public function store()
  {
    $dataRegistro = request()->input();

    $suscription = $this->suscriptionRepository->create($dataRegistro);

    return response([
      'suscription' => $suscription,
    ], Response::HTTP_OK);
  }

  /**
   * Obtiene los eventos de un usuario
  */
  public function byUser($user_id)
  {
    $suscriptions = $this->suscriptionRepository->findByUserId($user_id);
    return response([
      'suscriptions' => $suscriptions,
    ], Response::HTTP_OK);
  }
}
