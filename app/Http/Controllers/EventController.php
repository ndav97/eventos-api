<?php

namespace App\Http\Controllers;

use App\Models\Events\EventRepository;
use Firebase\JWT\JWT;
use Illuminate\Http\Response;

class EventController extends Controller
{
  protected $eventRepository;

  public function __construct(EventRepository $eventRepository)
  {
    $this->eventRepository = $eventRepository;
  }

  /**
   * Registro de evento
   */
  public function store()
  {
    $dataRegistro = request()->input();

    $event = $this->eventRepository->create($dataRegistro);

    return response([
      'event' => $event,
    ], Response::HTTP_OK);
  }

  /**
   * Obtiene todos los eventos
  */
  public function index()
  {
    $events = $this->eventRepository->index();

    return response([
      'events' => $events,
    ], Response::HTTP_OK);
  }

  /**
   * Obtiene los eventos de un usuario
  */
  public function byUser($id)
  {
    $events = $this->eventRepository->findByUserId($id);

    CheckArray($events->toArray(), 'Aún no has registrado ningún evento');

    return response([
      'events' => $events,
    ], Response::HTTP_OK);
  }
}
